﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]

public class MovingPlatforms : MonoBehaviour {

    [SerializeField]
    Vector3 movementVector;

    [Range(0,1)]
    [SerializeField]
    float movementFactor; //0 for not moved 1 for fully moved

    [SerializeField]
    float period = 2f;

    Vector3 startingPos;

    private void Start()
    {
        startingPos = transform.position;
    }

    private void Update()
    {
        if (period <= Mathf.Epsilon)
        {
            return;
        }

        float cycles = Time.time / period;

        const float tau = Mathf.PI * 2f;//about 6.28
        float rawSinWave = Mathf.Sin(cycles*tau);

        movementFactor = rawSinWave / 2f + 0.5f;

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
    }

}

