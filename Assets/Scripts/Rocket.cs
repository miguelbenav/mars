﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {
    

    [SerializeField]
    float rcsThrust = 100f;
    [SerializeField]
    float mainThrust = 100f;
    [SerializeField]
    AudioClip mainEngine;
    [SerializeField]
    AudioClip WinCue;
    [SerializeField]
    AudioClip DeathCue;
    //[SerializeField]
   // Camera mainCamera;

    [SerializeField]
    ParticleSystem mainEngineAnim;
    [SerializeField]
    ParticleSystem WinCueAnim;
    [SerializeField]
    ParticleSystem DeathCueAnim;

    Rigidbody rigidBody;
    AudioSource audioSource;
    //public LayerMask touchInputMask;


    enum State {Alive, Dying, Transcending};
    State state = State.Alive;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        
    }

    void Update()
    {
        if (state == State.Alive)
        {
            RespondToThrustInput();
            RespondToRotateInput();

        /*    foreach (Touch touch in Input.touches)
            {
                Ray ray = GetComponent<Camera>().ScreenPointToRay(touch.position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, touchInputMask))
                {
                    Screen.width
                }
            }*/
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive)
        {
            return;
        }
       switch (collision.gameObject.tag)
        {
            case "Friendly":
                // do nothing
                break;

            case "Unfriendly":
                Invoke("LoadFirstLevel", 1f);
                audioSource.PlayOneShot(DeathCue);
                DeathCueAnim.Play();
               
                state = State.Dying;
                break;

            case "Finish":
               Invoke("LoadNextLevel",1f);
                audioSource.PlayOneShot(WinCue);
                WinCueAnim.Play();
                state = State.Transcending;
                break;
        }
    }

    private void LoadFirstLevel()
    {
        SceneManager.LoadScene(0);
    

    }
        

    private void LoadNextLevel()
    {
        SceneManager.LoadScene(1);
    }

    private void RespondToThrustInput()
    {
        
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W))
        {
            ApplyThrust();

        }   else
        {
            audioSource.Stop();
            mainEngineAnim.Stop();
        }

    }



    private void RespondToRotateInput()
    {
        rigidBody.freezeRotation = true;

        

        float rotationThisFrame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A) || (Input.touches.Length> 0 && Input.touches[0].position.x<Screen.width/2f))
        {
           
            transform.Rotate(Vector3.forward * rotationThisFrame);
            ApplyThrust();
        }
        else if (Input.GetKey(KeyCode.D) || (Input.touches.Length > 0 && Input.touches[0].position.x > Screen.width / 2f))
        {
            transform.Rotate(-Vector3.forward * rotationThisFrame);
            ApplyThrust();
        }

        rigidBody.freezeRotation = false;
       
    }

       private void ApplyThrust()
    {
       float liftPower = mainThrust * Time.deltaTime;

        rigidBody.AddRelativeForce(Vector3.up * liftPower);

        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);

        }
        mainEngineAnim.Play();
     
    }
}
